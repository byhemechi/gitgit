"use strict";
const yaml = require("js-yaml");
const fs = require("fs-extra");
const express = require("express");
const app = express();
const Remarkable = require("remarkable");
const md = new Remarkable({
    html: true
});

app.set("view engine", "pug");

function renderStep(path, steps, res) {
    var page = steps;
    path.forEach(i => {
        page = page.children[i] || steps.children["404"];
    });
    page.body = md.render(page.body);
    res.render(__dirname + "/views/step", {page});
}

app.use("/", express.static(__dirname + "/public"));

app.use("/", function(req, res) {
    const steps = yaml.load(fs.readFileSync(__dirname + "/pages.yml"));
    var path = req.url.replace(/\.html$/i, "").split("/");
    path.splice(0, 1);
    if (path.length == 1 && path[0] == "") {
        renderStep(["index"], steps, res);
    } else {
        renderStep(path, steps, res);
    }
});

module.exports = app;