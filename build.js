const pug = require("pug");
const fs = require("fs-extra");
const step = pug.compileFile("views/step.pug");
const yaml = require("js-yaml");
const steps = yaml.load(fs.readFileSync(__dirname + "/pages.yml"));
const Remarkable = require("remarkable");
const md = new Remarkable({
    html: true
});

function renderStep(path) {
    // console.log(path.join("/"));
    var page = steps;
    path.forEach(i => {
        page = page.children[i];
    });
    page.body = md.render(page.body);
    // console.log(!!page);
    fs.writeFileSync("dist/" + path[path.length - 1] + ".html", step({page}));
}

function iterate(path) {
    var page = steps;
    if(path) {
        path.forEach(i => {
            page = page.children[i];
        });
    }
    if(page.children) for (let i in page.children) {
        var np = path || [];
        np.push(i);
        renderStep(np);
    }
}

if(!fs.existsSync("dist")) {
    fs.mkdirSync("dist");
}

iterate();
fs.copySync("public", "dist");

